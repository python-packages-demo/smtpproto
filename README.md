# smtpproto

Sans-io implementation of the (client-side) ESMTP protocol. https://github.com/agronholm/smtpproto

Not yet in PyPI (2020-03): https://pypi.org/search/?q=smtpproto